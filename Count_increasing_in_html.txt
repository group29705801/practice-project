<!DOCTYPE html>
<html>
<head>
<meta>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Count Button Clicks</title>
</head>
<body>
    <button id="colorButton">click here</button>
    <p>Button clicks: <span id="clickCount">0</span></p>

    <script>
        var clickCounter = 0;

        document.getElementById("colorButton").addEventListener("click", function() {
            updateClickCount();
        });
       function updateClickCount() {
            clickCounter++;
            document.getElementById("clickCount").textContent = clickCounter;
        }
    </script>
</body>
</html>
